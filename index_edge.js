/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'stage-1',
                            type: 'group',
                            rect: ['-201', '0', '1225', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-1',
                                type: 'image',
                                rect: ['201px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-1.jpg",'0px','0px']
                            },
                            {
                                id: 'edificios',
                                type: 'image',
                                rect: ['0px', '170px', '699px', '363px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"edificios.png",'0px','0px']
                            },
                            {
                                id: 'globo12',
                                symbolName: 'globo1',
                                type: 'rect',
                                rect: ['256', '20', '836', '607', 'auto', 'auto']
                            },
                            {
                                id: 'flechero',
                                type: 'group',
                                rect: ['213', '258', '345', '351', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'palo',
                                    type: 'image',
                                    rect: ['77px', '12px', '217px', '339px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"palo.png",'0px','0px']
                                },
                                {
                                    id: 'btn-der',
                                    type: 'image',
                                    rect: ['98px', '76px', '247px', '91px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"btn-der.png",'0px','0px']
                                },
                                {
                                    id: 'btn-izq',
                                    type: 'image',
                                    rect: ['0px', '0px', '247px', '91px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"btn-izq.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'titulo',
                                type: 'image',
                                rect: ['232px', '20px', '481px', '160px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"titulo.png",'0px','0px']
                            },
                            {
                                id: 'avatar_intro',
                                type: 'group',
                                rect: ['521', '180', '251', '435', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'avatar',
                                    type: 'image',
                                    rect: ['44px', '85px', '189px', '350px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"avatar.png",'0px','0px']
                                },
                                {
                                    id: 'questions',
                                    symbolName: 'questions',
                                    type: 'rect',
                                    rect: ['0px', '0px', '251', '124', 'auto', 'auto']
                                }]
                            },
                            {
                                id: 'btn_actividad',
                                type: 'image',
                                rect: ['1120px', '92px', '98px', '96px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_actividad.png",'0px','0px'],
                                userClass: "btn1"
                            },
                            {
                                id: 'btn_inst',
                                type: 'image',
                                rect: ['1126px', '11px', '86px', '73px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"btn_inst.png",'0px','0px'],
                                userClass: "btn1"
                            }]
                        },
                        {
                            id: 'stage-2',
                            type: 'group',
                            rect: ['0', '0px', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidad',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                opacity: '0.8551724137931',
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.jpg",'0px','0px']
                            },
                            {
                                id: 'box1',
                                type: 'group',
                                rect: ['97', '24', '415', '581', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'cajon',
                                    type: 'image',
                                    rect: ['0px', '0px', '415px', '581px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"cajon.png",'0px','0px']
                                },
                                {
                                    id: 'texto-1',
                                    type: 'text',
                                    rect: ['24px', '28px', '368px', '473px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​</p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-weight: 700; font-size: 40px;\">Introducción</span></p><p style=\"margin: 0px; text-align: center;\">​<br></p><p style=\"margin: 0px; text-align: center;\">Cada paso que da como servidor público tiene gran impacto en la vida de muchos ciudadanos. Así que es necesario interiorizar los valores necesarios para alcanzar la integridad y generar un impacto positivo en todos aquellos que se encuentran a su alrededor.</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'texto-2',
                                    type: 'text',
                                    rect: ['24px', '28px', '368px', '473px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: center;\">​</p><p style=\"margin: 0px; text-align: center;\"><span style=\"font-size: 40px; font-weight: 700;\">Instrucción&nbsp;</span></p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px;\">​</p><p style=\"margin: 0px; text-align: center;\">En este juego lo invitamos a seguir lo que le indique su consciencia y lo que siente que se necesita para ser un buen servidor público. Elija el camino que quiere seguir teniendo en cuenta los valores sugeridos.</p>",
                                    font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'cir1',
                                    type: 'ellipse',
                                    rect: ['175px', '524px', '26px', '26px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,192,192,1)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                },
                                {
                                    id: 'cir2',
                                    type: 'ellipse',
                                    rect: ['221px', '524px', '26px', '26px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    borderRadius: ["50%", "50%", "50%", "50%"],
                                    fill: ["rgba(192,192,192,1)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"]
                                }]
                            },
                            {
                                id: 'avatar-2',
                                type: 'image',
                                rect: ['562px', '27px', '416px', '667px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"avatar22.png",'0px','0px']
                            },
                            {
                                id: 'close',
                                type: 'image',
                                rect: ['944px', '16px', '62px', '63px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"close.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage-3',
                            type: 'group',
                            rect: ['-2352px', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-movil',
                                type: 'group',
                                rect: ['0', '0', '6000', '640', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'fondo-2',
                                    type: 'image',
                                    rect: ['0px', '0px', '6000px', '640px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"fondo-2.jpg",'0px','0px']
                                },
                                {
                                    id: 'btn-der_1',
                                    type: 'group',
                                    rect: ['3178', '311', '186', '266', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-der-1',
                                        type: 'image',
                                        rect: ['1px', '51px', '179px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-1.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-positiva-1',
                                        type: 'image',
                                        rect: ['0px', '0px', '186px', '49px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-positiva-1.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-der_2',
                                    type: 'group',
                                    rect: ['3549', '217', '187', '263', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-der-2',
                                        type: 'image',
                                        rect: ['5px', '48px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-2.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-positiva-2',
                                        type: 'image',
                                        rect: ['0px', '0px', '187px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-positiva-2.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-der_3',
                                    type: 'group',
                                    rect: ['3912', '304', '299', '262', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-der-3',
                                        type: 'image',
                                        rect: ['61px', '47px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn3.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-positiva-3',
                                        type: 'image',
                                        rect: ['0px', '0px', '299px', '44px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-positiva-3.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-der_4',
                                    type: 'group',
                                    rect: ['4290', '100', '249', '270', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-der-4',
                                        type: 'image',
                                        rect: ['36px', '55px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-4.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-positiva-4',
                                        type: 'image',
                                        rect: ['0px', '4px', '249px', '48px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-positiva-4.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-der_5',
                                    type: 'group',
                                    rect: ['4622', '320', '250', '265', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-der-5',
                                        type: 'image',
                                        rect: ['38px', '50px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-5.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-positiva-5',
                                        type: 'image',
                                        rect: ['0px', '0px', '250px', '48px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-positiva-5.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-der_6',
                                    type: 'group',
                                    rect: ['5023', '128', '250', '262', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-der-6',
                                        type: 'image',
                                        rect: ['37px', '47px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-6.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-positiva-6',
                                        type: 'image',
                                        rect: ['0px', '0px', '250px', '48px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-positiva-6.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'img-1-positiva',
                                    type: 'image',
                                    rect: ['5292px', '150px', '786px', '433px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"img-1-positiva.png",'0px','0px']
                                },
                                {
                                    id: 'img-2-positiva',
                                    type: 'image',
                                    rect: ['5292px', '136px', '786px', '453px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"img-2-positiva.png",'0px','0px']
                                },
                                {
                                    id: 'btn-izq_1',
                                    type: 'group',
                                    rect: ['2394', '309', '249', '264', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-izq-1',
                                        type: 'image',
                                        rect: ['32px', '49px', '179px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-1.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-negativa-1',
                                        type: 'image',
                                        rect: ['0px', '0px', '249px', '49px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-negativa-1.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-izq_2',
                                    type: 'group',
                                    rect: ['2042', '204', '250', '266', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-izq-2',
                                        type: 'image',
                                        rect: ['36px', '51px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-2.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-negativa-2',
                                        type: 'image',
                                        rect: ['0px', '0px', '250px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-negativa-2.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-izq_3',
                                    type: 'group',
                                    rect: ['1599', '318', '250', '269', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-izq-3',
                                        type: 'image',
                                        rect: ['36px', '54px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn3.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-negativa-3',
                                        type: 'image',
                                        rect: ['0px', '0px', '250px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-negativa-3.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-izq_4',
                                    type: 'group',
                                    rect: ['1160', '118', '322', '267', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-izq-4',
                                        type: 'image',
                                        rect: ['72px', '52px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-4.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-negativa-4',
                                        type: 'image',
                                        rect: ['0px', '0px', '322px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-negativa-4.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-izq_5',
                                    type: 'group',
                                    rect: ['845', '312', '249', '266', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-izq-5',
                                        type: 'image',
                                        rect: ['36px', '51px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-5.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-negativa-5',
                                        type: 'image',
                                        rect: ['0px', '0px', '249px', '49px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-negativa-5.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'btn-izq_6',
                                    type: 'group',
                                    rect: ['525', '111', '250', '268', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'btn-izq-6',
                                        type: 'image',
                                        rect: ['36px', '53px', '178px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-6.png",'0px','0px']
                                    },
                                    {
                                        id: 'palabra-negativa-6',
                                        type: 'image',
                                        rect: ['0px', '0px', '250px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palabra-negativa-6.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'img-1-Integridad-Negativa',
                                    type: 'image',
                                    rect: ['-36px', '58px', '785px', '511px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"img-1-Integridad-Negativa.png",'0px','0px']
                                },
                                {
                                    id: 'img-2-Integridad-Negativa',
                                    type: 'image',
                                    rect: ['-37px', '147px', '786px', '415px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"img-2-Integridad-Negativa.png",'0px','0px']
                                },
                                {
                                    id: 'flechas',
                                    type: 'group',
                                    rect: ['2677', '318', '220', '233', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'palo2',
                                        type: 'image',
                                        rect: ['62px', '18px', '138px', '215px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"palo2.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-der2',
                                        type: 'image',
                                        rect: ['44px', '59px', '176px', '66px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-der2.png",'0px','0px']
                                    },
                                    {
                                        id: 'btn-izq2',
                                        type: 'image',
                                        rect: ['0px', '0px', '176px', '66px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"btn-izq2.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'globo2',
                                symbolName: 'globo2',
                                type: 'rect',
                                rect: ['2893', '84', '253', '493', 'auto', 'auto']
                            },
                            {
                                id: 'nombre-curso',
                                type: 'image',
                                rect: ['3105px', '-5px', '277px', '125px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"nombre-curso.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'stage-4',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-opacidad2',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                opacity: '0.89655172413793',
                                fill: ["rgba(0,0,0,0)",im+"fondo-opacidad.jpg",'0px','0px']
                            },
                            {
                                id: 'avatar2',
                                type: 'image',
                                rect: ['118px', '63px', '355px', '676px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"avatar2.png",'0px','0px']
                            },
                            {
                                id: 'box2',
                                type: 'group',
                                rect: ['512', '100', '415', '413', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box_2',
                                    type: 'image',
                                    rect: ['0px', '0px', '415px', '413px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"box2.png",'0px','0px']
                                },
                                {
                                    id: 'texto2',
                                    type: 'text',
                                    rect: ['30px', '35px', '355px', '318px', 'auto', 'auto'],
                                    text: "<p style=\"margin: 0px; text-align: justify;\">​Recuerde que nuestros principios, direccionan nuestro actuar. Un hombre con principios siempre actuará de manera ejemplar, correcta y clara. Un hombre sin principios actuará de manera equivocada, vergonzosa y censurable.</p>",
                                    font: ['Arial, Helvetica, sans-serif', [18, "px"], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                                },
                                {
                                    id: 'home',
                                    type: 'image',
                                    rect: ['282px', '247px', '103px', '137px', 'auto', 'auto'],
                                    cursor: 'pointer',
                                    fill: ["rgba(0,0,0,0)",im+"home.png",'0px','0px']
                                }]
                            },
                            {
                                id: 'close2',
                                type: 'image',
                                rect: ['947px', '16px', '62px', '63px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"close2.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 2500,
                    autoPlay: true,
                    data: [
                        [
                            "eid93",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flechero}",
                            [50,94],
                            [50,94],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid564",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flechero}",
                            [50,94],
                            [50,94],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid565",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flechero}",
                            [50,94],
                            [50,94],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid566",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${flechero}",
                            [50,94],
                            [50,94],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid567",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${flechero}",
                            [50,94],
                            [50,94],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid568",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${flechero}",
                            [50,94],
                            [50,94],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            },
            "globo1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'group',
                            id: 'globo1',
                            rect: ['0px', '0px', '836', '607', 'auto', 'auto'],
                            c: [
                            {
                                transform: [[], ['-3'], [0, 0, 0], [1, 1, 1]],
                                id: 'linea',
                                type: 'image',
                                rect: ['0px', '500px', '607px', '107px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/linea.png', '0px', '0px']
                            },
                            {
                                type: 'image',
                                id: 'globo',
                                rect: ['529px', '0px', '307px', '597px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/globo.png', '0px', '0px']
                            }]
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '836px', '607px']
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid44",
                            "rotateZ",
                            0,
                            805,
                            "linear",
                            "${linea}",
                            '-3deg',
                            '1deg'
                        ],
                        [
                            "eid47",
                            "rotateZ",
                            805,
                            1195,
                            "linear",
                            "${linea}",
                            '1deg',
                            '-3deg'
                        ],
                        [
                            "eid40",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid569",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid570",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid571",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid572",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid573",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid45",
                            "-webkit-transform-origin",
                            805,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid574",
                            "-moz-transform-origin",
                            805,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid575",
                            "-ms-transform-origin",
                            805,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid576",
                            "msTransformOrigin",
                            805,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid577",
                            "-o-transform-origin",
                            805,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid578",
                            "transform-origin",
                            805,
                            0,
                            "linear",
                            "${linea}",
                            [100,50],
                            [100,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid37",
                            "top",
                            0,
                            1000,
                            "linear",
                            "${globo}",
                            '0px',
                            '-16px'
                        ],
                        [
                            "eid39",
                            "top",
                            1000,
                            1000,
                            "linear",
                            "${globo}",
                            '-16px',
                            '0px'
                        ]
                    ]
                }
            },
            "questions": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '0px', '251px', '124px', 'auto', 'auto'],
                            id: 'preguntas',
                            opacity: '1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/preguntas.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '251px', '124px']
                        }
                    }
                },
                timeline: {
                    duration: 2500,
                    autoPlay: true,
                    data: [
                        [
                            "eid60",
                            "opacity",
                            0,
                            1000,
                            "linear",
                            "${preguntas}",
                            '1',
                            '0'
                        ],
                        [
                            "eid62",
                            "opacity",
                            1000,
                            1000,
                            "linear",
                            "${preguntas}",
                            '0',
                            '1'
                        ]
                    ]
                }
            },
            "globo2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            type: 'image',
                            id: 'globo2',
                            rect: ['0px', '0px', '253px', '493px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/globo2.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '253px', '493px']
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid141",
                            "top",
                            0,
                            1000,
                            "linear",
                            "${globo2}",
                            '0px',
                            '4px'
                        ],
                        [
                            "eid143",
                            "top",
                            1000,
                            1000,
                            "linear",
                            "${globo2}",
                            '4px',
                            '0px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");
