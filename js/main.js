var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Reto de la integridad",
    autor: "Edilson Laverde Molina",
    date: "22/07/2020",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];
Number.prototype.between = function(a, b) {
    var min = Math.min.apply(Math, [a, b]),
      max = Math.max.apply(Math, [a, b]);
    return this > min && this < max;
  };
var move=0;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                t.animation();
                t.events();
                t.keys();
                ivo(ST + "preload").hide();
                ivo(ST+"texto-1").show();
                ivo(ST+"texto-2").hide();
                stage1.play();
            });
        },
        methods: {
            reset:function () {
                move = 0;
                ivo(ST+"fondo-movil").css("left","0px");
                ivo(ST+"globo2").show();
                left.reverse();
                right.reverse();
            },
            events: function () {
                var t = this;
                ivo(ST+"btn_inst").on("click", function () {
                    ivo.play("clic");
                    stage2.play().timeScale(1);
                }).on("mouseover", function () {}).on("mouseout", function () {});

                
                ivo(ST+"home").on("click", function () {
                    window.location.href="https://virtual.ucundinamarca.edu.co/course/view.php?id=15147&section=1";
                }).on("mouseover", function () {}).on("mouseout", function () {});
                
                ivo(ST+"close").on("click", function () {
                    ivo.play("clic");
                    stage2.reverse().timeScale(5);
                }).on("mouseover", function () {}).on("mouseout", function () {});

                ivo(ST+"close2").on("click", function () {
                    ivo.play("clic");
                    stage4.reverse().timeScale(5);
                    t.reset();
                }).on("mouseover", function () {}).on("mouseout", function () {});

                ivo(ST+"btn_actividad").on("click", function () {
                    ivo.play("clic");
                    stage3.play().timeScale(1);
                }).on("mouseover", function () {}).on("mouseout", function () {});

                ivo(ST+"cir1").on("click", function () {
                    ivo.play("clic");
                    ivo(ST+"texto-1").show();
                    ivo(ST+"texto-2").hide();
                }).on("mouseover", function () {}).on("mouseout", function () {});
                $(ST+"cir2").addClass("animated infinite tada");
                ivo(ST+"cir2").on("click", function () {
                    ivo.play("clic");
                    $(ST+"cir2").removeClass("animated infinite tada");
                    ivo(ST+"texto-1").hide();
                    ivo(ST+"texto-2").show();
                }).on("mouseover", function () {}).on("mouseout", function () {});
            },
            keys(){
               
                const step=5;
                ivo(window).on("keydown", function (event) {
                    if(move<=2350){
                        if(event.keyCode==37){//izquierda
                            move+=step;
                            ivo(ST+"globo2").css("transform","scaleX(1)");
                        }
                    }else{
                        left.restart();
                        ivo(ST+"globo2").hide();
                    }
                    if(move>=-2520){
                        if(event.keyCode==39){//derecha
                            move-=step;
                            ivo(ST+"globo2").css("transform","scaleX(-1)");
                        }
                    }
                    if(move<-2520){
                        right.restart();
                        ivo(ST+"globo2").hide();
                    }
                    if(move.between(270,300)){
                        globo_izq_2.play();
                    }
                    if(move.between(680,700)){
                        globo_izq_3.play();
                    }
                    if(move.between(1130,1450)){
                        globo_izq_4.play();
                    }
                    if(move.between(1540,1650)){
                        globo_izq_5.play();
                    }
                    if(move.between(1900,2000)){
                        globo_izq_6.play();
                    }

                    if(move.between(-270,-300)){
                        globo_der_2.play();
                    }
                    if(move.between(-680,-700)){
                        globo_der_3.play();
                    }
                    
                    if(move.between(-1130,-1450)){
                        globo_der_4.play();
                    }
                    if(move.between(-1540,-1650)){
                        globo_der_5.play();
                    }
                    if(move.between(-1900,-2000)){
                        globo_der_6.play();
                    }
                    
                    ivo(ST+"fondo-movil").css("left",move+"px");
                }).on("mouseover", function () {}).on("mouseout", function () {});
            },
            globos:function(name,globo){
                window[name]= new TimelineMax()
                window[name].append(TweenMax.from(globo, .4, {y:-200,opacity:0}), 0); 
                window[name].stop();
            },
            animation: function () {
                let t = this;
                stage1 = new TimelineMax({onComplete:()=>{
                    setTimeout(()=>{stage2.play();},1000);
                }});
                stage1.append(TweenMax.from(ST + "stage-1", .8, {y: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "titulo", .8,  {x: 300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "flechero", .8,{scaleY: 0, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".btn1", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();

                stage2 = new TimelineMax();
                stage2.append(TweenMax.from(ST + "stage-2", .8, {y: 1300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "box1", .8,    {x: -300, opacity: 0}), 0);
                stage2.append(TweenMax.from(ST + "avatar-2", .8,{x: 300, opacity: 0}), -.4);
                stage2.append(TweenMax.from(ST + "close", .8,   {scale:0,rotation:900, opacity: 0}), 0);
                stage2.stop();

                stage3 = new TimelineMax();
                stage3.append(TweenMax.from(ST + "stage-3", .8,      {y: 1300, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "nombre-curso", .8, {y: -200, opacity: 0}), 0);
                stage3.append(TweenMax.from(ST + "globo2", .8, {y: 1300, opacity: 0}), 0);
                stage3.stop();

                stage4 = new TimelineMax();
                stage4.append(TweenMax.from(ST + "stage-4", .8, {y: 1300, opacity: 0}), 0);
                stage4.append(TweenMax.from(ST + "avatar2", .8, {x: -300, opacity: 0}), 0);
                stage4.append(TweenMax.from(ST + "box2", .8,    {y: 1300, opacity: 0}), 0);
                stage4.append(TweenMax.from(ST + "close2", .8,   {scale:0,rotation:900, opacity: 0}), 0);
                stage4.stop();

                left = new TimelineMax({onComplete:function(){
                    setTimeout(()=>{stage4.play().timeScale(1);},1000);
                }});
                left.append(TweenMax.from(ST + "img-1-Integridad-Negativa", 1, {opacity: 0}), 0);
                left.append(TweenMax.to(ST + "img-1-Integridad-Negativa", .8, {opacity: 0,delay:2}), 0);
                left.append(TweenMax.from(ST + "img-2-Integridad-Negativa", .8, {opacity: 0}), -1);
                left.stop();

                right = new TimelineMax({onComplete:function(){
                    setTimeout(()=>{stage4.play().timeScale(1);},1000);
                }});
                right.append(TweenMax.from(ST + "img-1-positiva", 1, {opacity: 0}), 0);
                right.append(TweenMax.to(ST + "img-1-positiva", .8, {opacity: 0,delay:2}), 0);
                right.append(TweenMax.from(ST + "img-2-positiva", .8, {opacity: 0}), );
                right.stop();
                for(let index=2; index<=6; index++){
                    t.globos("globo_izq_"+index,ST+"btn-izq_"+index);
                    t.globos("globo_der_"+index,ST+"btn-der_"+index);
                }
            }
        }
    });
}